import cv2
import os
import numpy as np
from PIL import Image

kamera = 1
Brightness = 145 
Width= 640                   
Height = 480  

cap = cv2.VideoCapture(kamera ,cv2.CAP_DSHOW)
##cap = cv2.VideoCapture(kamera)

cap.set(3, Width)
cap.set(4, Height)
    
akurasi = 20
dataset = 'kopi/'
cascade = cv2.CascadeClassifier('Biji kopi cascade.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('model/training1.yml')

jenis = {0:"Uknown"}
for path in os.listdir(dataset):
    id = int(os.path.split(dataset + path)[-1].split(".")[1])
    nama = os.path.split(dataset + path)[-1].split(".")[0]
    jenis[id] = nama
print(jenis)

while True:
    cap.set(10, Brightness)
    __, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    objects = cascade.detectMultiScale(gray,1.1,5)
    for (x,y,w,h) in objects:
        roi = gray[y:y+h,x:x+w]
        roi = cv2.resize(roi,(120,120))
        id, confidence = recognizer.predict(roi)
        if (confidence < 100 - akurasi):
            nama = jenis[id]
            persen = "  {0}%".format(round(100 - confidence))
        else:
            nama = jenis[0]
            persen = "  {0}%".format(round(100 - confidence))
        cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 2)
        cv2.putText(img, str(nama), (x+5,y-5),cv2.FONT_HERSHEY_COMPLEX_SMALL,1,(255,20,75),2)
        cv2.putText(img, str(persen), (x+5,y+h-5),cv2.FONT_HERSHEY_COMPLEX_SMALL,1,(255,20,75),2)
    cv2.imshow("gambar",img)

    if cv2.waitKey(10) & 0xFF == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        break
        
cap.release()
cv2.destroyAllWindows()
