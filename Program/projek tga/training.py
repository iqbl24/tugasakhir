import cv2
import os
import numpy as np

def img_augmentation(img): 
    imgs,hasil = [],[]
    h, w = img.shape
    center = (w / 2, h / 2)
    color = img[100,20]
    warna = (int(color), int(color), int(color))
    imgs.append(cv2.add(img, -10))
    imgs.append(cv2.add(img, 0))
    imgs.append(cv2.add(img, 10))
    imgs.append(cv2.add(img, 15))
    imgs.append(cv2.add(img, 20))
    for i in range(len(imgs)): 
        for a in range(9):
            rotasi = cv2.getRotationMatrix2D(center, a * 10, 1.0)
            hasil.append(cv2.warpAffine(imgs[i], rotasi, (w, h), borderValue=warna))
    return hasil

jenis = {0:"Uknown"}
gambar,ids = [],[]
folder = 'kopi/'
count = 0
for path in os.listdir(folder):
    img = cv2.imread(folder + path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray,(120,120))
    img = np.array(gray,'uint8')    

    id = int(os.path.split(folder + path)[-1].split(".")[1])
    nama = os.path.split(folder + path)[-1].split(".")[0]
    jenis[id] = nama
    for x in img_augmentation(img):    
        gambar.append(x)
        ids.append(id)
        cv2.imshow("gambar",x)
        cv2.waitKey(1)
        count += 1
    
    print("id : "+str(id) +' Nama : '+ str(nama))
print('Jenis : ',jenis)
print("Total gambar : ",len(gambar))
print("Id gambar    : ",ids)

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.train(gambar, np.array(ids))
print ('Jumlah gambar yang mau di training = '+str(len(gambar))+ " Gambar")
recognizer.write('model/training1.yml')
print("\n [INFO] {0} jenis biji kopi selesai di training.".format(len(np.unique(ids))))

cv2.destroyAllWindows()
