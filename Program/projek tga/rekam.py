import cv2
import os
import numpy as np
from PIL import Image

nama = "long berry"
id = 1

cascade = cv2.CascadeClassifier('Biji kopi cascade.xml')
kamera = 1
Brightness = 145
Width= 640                   
Height = 480  

cap = cv2.VideoCapture(kamera ,cv2.CAP_DSHOW)
cap.set(3, Width)
cap.set(4, Height)
c = 0

while True:
	cap.set(10, Brightness)
	__, img = cap.read()
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	objects = cascade.detectMultiScale(gray,1.1,5)
	for (x,y,w,h) in objects:
	    roi = img[y:y+h,x:x+w]

	    if cv2.waitKey(1) & 0xFF == ord('x'):
	        print("Foto capture : ",c," Gambar")
	        cv2.imwrite('kopi/'+str(nama)+'.'+str(id)+'.'+str(c)+'.jpg',roi)
	        cv2.imshow("gray",roi)
	        cv2.waitKey(1000)
	        cv2.destroyAllWindows()
	        print(str(nama)+'.'+str(id)+'.'+str(c)+'.jpg')
	        c += 1

	    cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 2)
	    cv2.putText(img, "jumlah : "+str(c), (x+5,y-5),cv2.FONT_HERSHEY_COMPLEX_SMALL,1,(255,0,225),2)
	cv2.imshow("gambar",img)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		cap.release()
		cv2.destroyAllWindows()
		break

cap.release()
cv2.destroyAllWindows()
